package com.dejanmarkovic.diario.service;

import android.app.IntentService;
import android.content.Intent;

/**
 * Created by dejanmarkovic on 3/1/17.
 */
public class NotificationService extends IntentService{

    public NotificationService() {
        super("Notification service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String intentType = intent.getExtras().getString("caller");
        if(intentType == null) return;
        //handle other types of callers, like a notification.
    }
}
