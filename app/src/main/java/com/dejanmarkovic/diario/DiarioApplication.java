package com.dejanmarkovic.diario;

import android.app.Application;
import android.content.ContextWrapper;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.pixplicity.easyprefs.library.Prefs;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;

/**
 * Created by dejanmarkovic on 11/2/16.
 */

public class DiarioApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // Initialize the Prefs class
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();

        TwitterAuthConfig authConfig = new TwitterAuthConfig("x0i2zmIz7bz0n28xCuHRCVsYO", "HcBr4FJnGIhYoGwrYGI5Ptimsbwuuius6GSZP9JFRgB4v23psO");

        Fabric.with(this, new Twitter(authConfig), new Crashlytics());

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }
}
