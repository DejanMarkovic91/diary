package com.dejanmarkovic.diario.activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.widget.ProgressBar;

import com.dejanmarkovic.diario.R;

/**
 * Prikazuje se kada aplikacija radi nesto, a neophodno je blokirati GUI.
 *
 * @author Dejan Markovic
 */
public class SpinnerDialog extends Dialog {

    public SpinnerDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.dialog_progress);
        ((ProgressBar) findViewById(R.id.progressBar))
                .getIndeterminateDrawable()
                .setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        setCancelable(false);
    }
}
