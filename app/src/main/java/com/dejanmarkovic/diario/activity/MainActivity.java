package com.dejanmarkovic.diario.activity;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.dejanmarkovic.diario.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.p_v.flexiblecalendar.FlexibleCalendarView;
import com.p_v.flexiblecalendar.entity.Event;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.calendar_view)
    FlexibleCalendarView calendarView;
    @BindView(R.id.month)
    TextView monthName;

    @OnClick(R.id.previous)
    public void previosMonth() {
        calendarView.moveToPreviousMonth();
    }

    @OnClick(R.id.next)
    public void nextMonth() {
        calendarView.moveToNextMonth();
    }

    @OnClick(R.id.month)
    public void moveToCurrentMonth() {
        calendarView.goToCurrentMonth();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        monthName.setText(String.format(Locale.getDefault(), "%s (%d)", getMonthName(calendarView.getCurrentMonth()), calendarView.getCurrentYear()));

        calendarView.setOnDateClickListener(new FlexibleCalendarView.OnDateClickListener() {
            @Override
            public void onDateClick(int year, int month, int day) {
                Intent intent = new Intent(MainActivity.this, NoteActivity.class);
                intent.putExtra("YEAR", year);
                intent.putExtra("MONTH", month);
                intent.putExtra("DAY", day);
                startActivity(intent);
            }
        });

        calendarView.setOnMonthChangeListener(new FlexibleCalendarView.OnMonthChangeListener() {
            @Override
            public void onMonthChange(int year, int month, int direction) {
                monthName.setText(String.format(Locale.getDefault(), "%s (%d)", getMonthName(month), year));
            }
        });

        calendarView.setEventDataProvider(new FlexibleCalendarView.EventDataProvider() {
            @Override
            public List<CustomEvent> getEventsForTheDay(int year, int month, int day) {
                return getEventColorList(year, month, day);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        calendarView.refresh();
    }

    private List<CustomEvent> getEventColorList(int year, int month, final int day) {
        List<CustomEvent> eventsList = new ArrayList<>();
        final String dateText = day + "_" + (month + 1) + "_" + year + "_";

        if (Prefs.getBoolean(dateText, false)) {
            eventsList.add(new CustomEvent(android.R.color.holo_purple));
        } else {
            if(!Prefs.contains(dateText)) {
                try {
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference ref = database.getReference().child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dateText);
                    ref.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Prefs.putBoolean(dateText, dataSnapshot.getValue() != null);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                } catch (NullPointerException e) {
                    try {
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference ref = database.getReference().child(FirebaseAuth.getInstance().getCurrentUser().getUid());
                        ref.push();
                    } catch (Exception ex) {
                        return eventsList;
                    }
                }
            }
        }
        return eventsList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.logout:
                FirebaseAuth.getInstance().signOut();
                Prefs.putString(LoginActivity.USERNAME, null);
                Prefs.putString(LoginActivity.PASSWORD, null);
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                return true;
            case R.id.about:
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
                builder.setTitle(R.string.about);
                builder.setMessage(R.string.about_message);
                builder.setPositiveButton(android.R.string.ok, null);
                builder.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String getMonthName(int month) {
        return getResources().getStringArray(R.array.month_names)[month];
    }

    public class CustomEvent implements Event {

        private int color;

        public CustomEvent(int color) {
            this.color = color;
        }

        @Override
        public int getColor() {
            return color;
        }
    }
}
