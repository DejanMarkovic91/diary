package com.dejanmarkovic.diario.activity;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.crashlytics.android.Crashlytics;
import com.dejanmarkovic.diario.R;
import com.dejanmarkovic.diario.receivers.NotificationBroadcastReceiver;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NoteActivity extends AppCompatActivity {

    public static final int DELETE = 1;
    public static final int EDIT = 2;
    @BindView(R.id.notes)
    RecyclerView notesList;
    @BindView(R.id.toolbar)
    Toolbar date;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    private List<Note> notes = new ArrayList<>();
    private NoteAdapter adapter;
    private String dateText;
    private int day, month, year;

    @OnClick(R.id.add)
    public void addNote() {
        setNoteAndOpenDialog(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        ButterKnife.bind(this);
        setSupportActionBar(date);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        year = intent.getIntExtra("YEAR", 0);
        month = intent.getIntExtra("MONTH", 0) + 1;
        day = intent.getIntExtra("DAY", 0);

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        notesList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        notesList.setAdapter(adapter = new NoteAdapter());

        dateText = day + "_" + month + "_" + year + "_";
        this.collapsingToolbar.setTitle(dateText.replaceAll("_", "."));

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference ref = database.getReference().child(currentUser.getUid()).child(dateText);
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Note note = dataSnapshot.getValue(Note.class);
                if (!notes.contains(note)) {
                    notes.add(note);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                //do nothing
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                //do nothing
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Crashlytics.log(databaseError.getMessage());
            }
        });
    }

    public void saveNotes() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference().child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dateText);
        ref.setValue(notes);
        ref.push();
    }

    public void submitNote(Note note) {
        notes.remove(note);
        notes.add(note);
        saveNotes();
        adapter.notifyDataSetChanged();
    }

    class NoteAdapter extends RecyclerView.Adapter<ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.position = position;
            holder.text.setText(notes.get(holder.position).getText());
        }

        @Override
        public int getItemCount() {
            return notes.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView text;
        ImageButton share;
        ImageButton notify;
        ImageButton edit;
        ImageButton delete;

        int position;

        ViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.text);
            share = (ImageButton) itemView.findViewById(R.id.share);
            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShareCompat.IntentBuilder
                            .from(NoteActivity.this) // getActivity() or activity field if within Fragment
                            .setText(notes.get(position).getText())
                            .setType("text/plain") // most general text sharing MIME type
                            .setChooserTitle(R.string.share_note)
                            .startChooser();
                }
            });
            edit = (ImageButton) itemView.findViewById(R.id.edit);
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setNoteAndOpenDialog(notes.get(position));
                }
            });
            delete = (ImageButton) itemView.findViewById(R.id.delete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(NoteActivity.this, R.style.AlertDialogStyle);
                    builder.setTitle(R.string.confirm_deletion);
                    builder.setMessage(R.string.delete_message);
                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Note note = notes.get(position);
                            notes.remove(position);
                            if (notes.isEmpty()) {
                                Calendar cal = Calendar.getInstance();
                                cal.setTimeInMillis(note.getCreated());
                                final String dateText = cal.get(Calendar.DATE) + "_" + cal.get(Calendar.MONTH) + "_" + cal.get(Calendar.YEAR) + "_";
                                Prefs.putBoolean(dateText, false);
                            }
                            saveNotes();
                        }
                    });
                    builder.setNegativeButton(android.R.string.no, null);
                    builder.show();
                }
            });
            notify = (ImageButton) itemView.findViewById(R.id.notify);
            notify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    final Calendar calendar = Calendar.getInstance();
                    int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    int minute = calendar.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker = new TimePickerDialog(view.getContext(),
                            new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                    calendar.set(year, month, day, selectedHour, selectedMinute);
                                    NotificationBroadcastReceiver.scheduleNotification(
                                            view.getContext(),
                                            calendar.getTimeInMillis(),
                                            1);//always same id
                                }
                            }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle(view.getContext().getString(R.string.time_for_notification));
                    mTimePicker.setCancelable(true);
                    mTimePicker.show();
                }
            });
        }
    }

    public void setNoteAndOpenDialog(final Note note) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
        builder.setTitle(note != null ? R.string.new_note : R.string.edit_note);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);//no autocorrect
        input.setMinHeight(150);
        input.setSingleLine(false);
        input.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        input.setLines(5);
        input.setMaxLines(10);
        input.setVerticalScrollBarEnabled(true);
        input.setMovementMethod(ScrollingMovementMethod.getInstance());
        input.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
        input.setTextColor(Color.WHITE);
        builder.setView(input);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (note != null) {
                    note.setText(input.getText().toString());
                    submitNote(note);
                } else {
                    Note newNote = new Note();
                    Calendar cal = Calendar.getInstance();
                    cal.set(year, month, day);
                    newNote.setCreated(cal.getTimeInMillis());
                    Prefs.putBoolean(dateText, true);
                    newNote.setText(input.getText().toString());
                    submitNote(newNote);
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();

        //must be after creation
        if (note != null) {
            input.setText(note.getText());
            input.setSelection(input.getText().length());
        }
    }
}
