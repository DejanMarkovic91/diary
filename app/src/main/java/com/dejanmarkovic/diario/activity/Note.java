package com.dejanmarkovic.diario.activity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by dejanm on 03-Nov-16.
 */

public class Note implements Parcelable {

    private long created;
    private String text;

    public Note() {

    }

    public Note(long created, String text) {
        this.created = created;
        this.text = text;
    }

    protected Note(Parcel in) {
        created = in.readLong();
        text = in.readString();
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(created);
        dest.writeString(text);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Note note = (Note) o;

        if (created != note.created) return false;
        return text != null ? text.equals(note.text) : note.text == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (created ^ (created >>> 32));
        result = 31 * result + (text != null ? text.hashCode() : 0);
        return result;
    }
}
